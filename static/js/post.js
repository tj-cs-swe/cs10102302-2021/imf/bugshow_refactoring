// 刷新页面清除保存在sessionStorage中的原始数据
$(function () {
    sessionStorage.setItem('md', '');
})

function cancleReply() {
    $("#commentBtn").removeAttr('hidden');
    $("#replyBtn").attr('hidden', 'hidden');
    $("#cancleReplyBtn").attr('hidden', 'hidden');
    $("#replyUserP").html('');
    $("#replyUserP").attr('hidden', 'hidden');
}

function replyComment() {
    comment = isEmpty();
    if (!comment){
        return false;
    }
    commentId = sessionStorage.getItem('commentId');
    commentUserId = sessionStorage.getItem('commentUserId');
    postId = $("#postTitle").data('id');
    $.ajax({
        type: "post",
        url: "/post/reply-comment/",
        data: {"post_id":postId, "comment_id": commentId, "comment_user_id": commentUserId, "comment": comment},
        success: function (res) {
            window.location.reload();
        }
    })
}

function reply(commentId, commentUser, commentUserId) {
    $("#commentBtn").attr('hidden', 'hidden');
    $("#replyBtn").removeAttr('hidden');
    $("#cancleReplyBtn").removeAttr('hidden');
    $("#replyUserP").removeAttr('hidden');
    $("#replyUserP").html('@'+commentUser);
    $("#replyUserP").attr('href', '/profile/user/'+commentUserId+'/');
    sessionStorage.setItem('commentId', commentId);
    sessionStorage.setItem('commentUserId', commentUserId);
    $('html,body').animate({ scrollTop: $("#commentPosition").offset().top - 100 }, 200)
}

var emoji_tag = $("#emoji-list img");
emoji_tag.click(function() {
    var e = $(this).data('emoji');
    $("#commentContent").val($("#commentContent").val() + e);
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

// 预览评论
function getMarkDownData() {
    let comment = $("#commentContent").val();
    if (comment == ''){
        $("#previewHtml").html('<p class="text-muted"><b>你还没有输入评论,你预览NM呢?</b></p>')
        return false;
    }
    let last = sessionStorage.getItem('md');
    // 评论没有修改时,不发送预览请求
    if (last){
        if (last == comment){
            return false;
        }
    }
    sessionStorage.setItem('md', comment);
    $.ajax({
        type:"post",
        url: '/normal/comment/render-md/',
        data: {'md': comment},
        success: function (res) {
            $("#previewHtml").html(res.html);
        }
    })
}

// 评论框按键监听事件
function tab(obj){
    if (event.keyCode == 9){
        event.preventDefault();
        var indent = '    ';
        var start = obj.selectionStart;
        var end = obj.selectionEnd;
        var selected = window.getSelection().toString();
        selected = indent + selected.replace(/\n/g, '\n' + indent);
        obj.value = obj.value.substring(0, start) + selected
            + obj.value.substring(end);
        obj.setSelectionRange(start + indent.length, start
            + selected.length);
    }
}

function isEmpty() {
    let comment = $("#commentContent").val();
    // 防止一直按回车键输入空白的评论内容
    if (comment.replace(/<br>/g, '').replace(/\s*/g, '') == ''){
        $(".p-error-hint").slideDown(500);
        $(".p-error-hint").show().delay(2000).hide(500);
        return false;
    }
    if (!comment){
        $(".p-error-hint").slideDown(500);
        $(".p-error-hint").show().delay(2000).hide(500);
        return false;
    }
    return comment;
}

// 提交评论
function postComment() {
    comment = isEmpty();
    if (!comment){
        return false;
    }
    let postId = $("#postTitle").data("id");
    $.ajax({
        type:"post",
        data: {"commentContent": comment, 'postId': postId},
        url: "/post/post-comment",
        success: function (res) {
            window.location.reload();
        }
    })
}

function submitDelete(commId) {
    $.ajax({
        url:'/post/delete-comment',
        type: 'POST',
        data: {'comm_id': commId},
        success: function (res){
            if (res.tag){
                window.location.reload();
            }else {
                alert("删除评论失败!");
                return false;
            }
        },
        error: function (){}
    })
}

$("#confirm-delete").on("show.bs.modal", function (e){
    let commID = $(e.relatedTarget).data('commid');
    $("#deleteBtn").val(commID);
})

function upload() {
    $("#uploadInput").click();
}

function uploadImage() {
    let img = $("#uploadInput")[0].files[0];
    let formdata = new FormData();
    formdata.append('file', img);
    $.ajax({
        url:"/normal/ajax-upload",
        type: "post",
        async: false,
        data: formdata,
        processData: false,
        contentType: false,
        success: function (res) {
            insertText(document.getElementById('commentContent'), res.imgPath)
        }
    })
}

// 在textarea中插入上传的图片markdown的值
function insertText(obj, str) {
    if (document.selection) {
        var sel = document.selection.createRange();
        sel.text = str;
    } else if (typeof obj.selectionStart === 'number' && typeof obj.selectionEnd === 'number') {
        var startPos = obj.selectionStart,
            endPos = obj.selectionEnd,
            cursorPos = startPos,
            tmpStr = obj.value;
        obj.value = tmpStr.substring(0, startPos) + str + tmpStr.substring(endPos, tmpStr.length);
        cursorPos += str.length;
        obj.selectionStart = obj.selectionEnd = cursorPos;
    } else {
        obj.value += str;
    }
}