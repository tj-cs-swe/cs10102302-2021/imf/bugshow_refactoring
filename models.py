from werkzeug.security import generate_password_hash, check_password_hash
from flask_avatars import Identicon
from extensions import db
from flask_login import UserMixin, current_user
from settings import *
import datetime

# 用户
class User(db.Model, UserMixin):
    __tablename__ = 't_user'

    id = db.Column(db.INTEGER, primary_key=True, nullable=False, index=True, autoincrement=True)
    username = db.Column(db.String(40), nullable=False, index=True, unique=True, comment='user name')
    nickname = db.Column(db.String(40), nullable=False, unique=True, comment='user nick name')
    password = db.Column(db.String(256), comment='user password')
    email = db.Column(db.String(128), unique=True, nullable=False, comment='user register email')
    avatar = db.Column(db.String(1000), nullable=False, comment='user avatar')
    role_id = db.Column(db.INTEGER, db.ForeignKey('t_role.id'), default=3, comment='user role id default is 3, that is student role')
    role = db.relationship('Role', back_populates='user')
    # 未读消息数
    unreadnotice = db.Column(db.INTEGER,nullable=False,default=0)
    # 收藏帖子数
    starpost = db.Column(db.INTEGER,nullable=False,default=0)

    # 设置密码
    def set_password(self, pwd):
        self.password = generate_password_hash(pwd)

    # 检查密码
    def check_password(self, pwd):
        return check_password_hash(self.password, pwd)

    # ip地址哈希随机生成初始化头像
    def generate_avatar(self):
        icon = Identicon()
        files = icon.generate(self.username)
        self.avatar = "resources/avatars/" + files[2]
    
    # 设置头像
    def set_avatar(self,avatar):
        self.avatar = avatar

# 用户角色
class Role(db.Model):
    __tablename__ = 't_role'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True, index=True)
    name = db.Column(db.String(40), nullable=False)
    user = db.relationship('User', back_populates='role', cascade='all')

# 讨论问题
class Problem(db.Model):
    __tablename__ = 't_problem'
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    name = db.Column(db.String(40), nullable=False)
    # 建立问题与帖子之间的外键级联
    posts = db.relationship('Post', cascade='all')

# 帖子
class Post(db.Model):
    __tablename__ = 't_post'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True, index=True)
    title = db.Column(db.String(100), index=True, nullable=False)
    content = db.Column(db.TEXT, nullable=False)
    textplain = db.Column(db.TEXT, nullable=False)
    # 问题外键
    pro_id = db.Column(db.INTEGER, db.ForeignKey('t_problem.id'))
    # 作者外键
    author_id = db.Column(db.INTEGER, db.ForeignKey('t_user.id'))
    star = db.Column(db.INTEGER, default=0, comment='star the answer')
    like = db.Column(db.INTEGER, default=0, comment='likes from people')
    fork = db.Column(db.INTEGER, default=0, comment='forks from people')
    # 创建修改浏览时间
    create_time = db.Column(db.DateTime, default=datetime.datetime.now)
    update_time = db.Column(db.DateTime, default=datetime.datetime.now)
    display_time = db.Column(db.DateTime, default=datetime.datetime.now)
    # 建立帖子与评论、喜爱帖子、星标的全部映射级联
    comments = db.relationship('Comments', cascade='all')
    likelog = db.relationship('LikeLog', cascade='all')
    forklog = db.relationship("ForkLog", cascade='all')
    # 建立帖子与问题、用户之间的外键
    pro = db.relationship('Problem')
    user = db.relationship('User')

    # 删除条件，帖子属于本人
    def can_delete(self):
        return current_user.id == self.author_id

# 评论
class Comments(db.Model):
    __tablename__ = 't_comments'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    body = db.Column(db.Text)
    replied_id = db.Column(db.INTEGER, db.ForeignKey('t_comments.id'))
    author_id = db.Column(db.INTEGER, db.ForeignKey('t_user.id'))
    post_id = db.Column(db.INTEGER, db.ForeignKey('t_post.id'))

    # 建立评论与帖子、作者、回复之间的外键
    post = db.relationship('Post')
    author = db.relationship('User')
    replies = db.relationship('Comments', cascade='all')
    replied = db.relationship('Comments', remote_side=[id])

    # 删除条件，帖子属于本人
    def can_delete(self):
        return current_user.id == self.author_id

# 验证码
class VerifyCode(db.Model):
    __tablename__ = 't_ver_code'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    val = db.Column(db.INTEGER, nullable=False)
    timestamps = db.Column(db.DateTime, default=datetime.datetime.now)
    expire_time = db.Column(db.DateTime, nullable=False)
    is_work = db.Column(db.Boolean, default=True)
    who = db.Column(db.String(40), nullable=False, comment='this ver code belong who')

# 通知
class Notice(db.Model):
    __tablename__ = 't_notice'
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True, index=True)
    title = db.Column(db.String(100), index=True, nullable=False)
    content = db.Column(db.TEXT, nullable=False)
    author_id = db.Column(db.INTEGER, db.ForeignKey('t_user.id'))
    status = db.Column(db.Enum('edit','check','post'),nullable=False,server_default='edit')
    # 建立与用户、通知状态之间的外键关系
    author = db.relationship('User')
    noticestatus = db.relationship('NoticeStatus', cascade='all')

# 帖子被喜爱
class LikeLog(db.Model):
    __tablename__ = 't_likestate'

    user_id = db.Column(db.INTEGER, db.ForeignKey('t_user.id'),primary_key=True)
    post_id = db.Column(db.INTEGER, db.ForeignKey('t_post.id'),primary_key=True)

# 帖子被星标
class ForkLog(db.Model):
    __tablename__ = 't_startstate'

    user_id = db.Column(db.INTEGER, db.ForeignKey('t_user.id'),primary_key=True)
    post_id = db.Column(db.INTEGER, db.ForeignKey('t_post.id'),primary_key=True)

# 通知状态
class NoticeStatus(db.Model):
    __tablename__ = 't_noticestatus'
    # 联合主键
    user_id = db.Column(db.INTEGER,db.ForeignKey('t_user.id'),primary_key=True)
    notice_id = db.Column(db.INTEGER,db.ForeignKey('t_notice.id'),primary_key=True)
    status = db.Column(db.Enum('read','unread','delete'),nullable=False,server_default='unread')

    # 与用户外键
    user = db.relationship('User')