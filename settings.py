import os

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

class BaseConfig(object):
    # SQLAlchemy设置追踪修改
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # 内置ck编辑器参数设置
    CKEDITOR_SERVE_LOCAL = True
    CKEDITOR_ENABLE_CODESNIPPET = True
    CKEDITOR_HEIGHT = 400
    CKEDITOR_FILE_UPLOADER = 'normal.image_upload'
    # 本机MySQL的用户、密码、绑定ip、端口
    DATABASE_USER = "root"
    DATABASE_PWD  = "55226181"
    DATABASE_HOST = "127.0.0.1"
    DATABASE_PORT = "3306"
    # flask邮箱系统设置绑定的邮箱服务器、账号、密码、邮件格式参数
    BUGSHOW_MAIL_SUBJECT_PRE = 'BugShow'
    MAIL_SERVER= "smtp.qq.com"
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = "1193354569@qq.com"
    MAIL_PASSWORD = "hezadrhymfjhjcig"
    MAIL_DEFAULT_SENDER = ('BugShow Administrator', MAIL_USERNAME)
    # 页面预览参数
    PER_PAGE = 7
    # 文件上传后保存路径、头像路径
    UPLOAD_PATH = os.path.join(basedir, 'bugshow/static/resources')
    AVATARS_SAVE_PATH = UPLOAD_PATH + '/avatars/'
    FILE_PATH = UPLOAD_PATH + '/file/'
    # 头像尺寸、底色参数、本地保存路径
    AVATARS_IDENTICON_ROWS = 15
    AVATARS_IDENTICON_COLS = 15
    AVATARS_IDENTICON_BG = "#000000"
    AVATARS_SIZE_TUPLE = (50, 50, 50)
    AVATARS_SAVE_PATH = UPLOAD_PATH + '/avatars/'

class DevelopmentConfig(BaseConfig):
    # SQLAlchemy对象MySQL数据库建立连接
    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/se?charset=utf8mb4'.format(BaseConfig.DATABASE_USER, BaseConfig.DATABASE_PWD, BaseConfig.DATABASE_HOST)
    # 用加密随机串做这次服务器运行的秘钥
    SECRET_KEY = os.urandom(32)
