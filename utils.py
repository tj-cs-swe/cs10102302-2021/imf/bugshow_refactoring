import re
from markdown import extensions
from markdown.treeprocessors import Treeprocessor
import hashlib
import psutil

try:
    from urlparse import urlparse, urljoin
except ImportError:
    from urllib.parse import urlparse, urljoin

from flask import request, url_for, redirect

# 用户帖子、评论、收藏可查范围map
PANGU_DATE = '1970-01-01'
TIME_RANGE = {'全部': 1, '半年': 180, '一月': 30, '三天': 3, '隐藏': -1}

def get_text_plain(html_text):
    from bs4 import BeautifulSoup
    bs = BeautifulSoup(html_text, 'html.parser')
    return bs.get_text()