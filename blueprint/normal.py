from flask import Blueprint, send_from_directory, request, jsonify
from extensions import db
from emails import send_email
from models import *
from utils import *
from settings import basedir
from flask_login import login_required
import datetime
import random
import os

# 日常管理蓝图
normal_bp = Blueprint('normal', __name__, url_prefix='/normal')

# 生成验证码
def generate_ver_code():
    str = ""
    for i in range(6):
        ch = chr(random.randrange(ord('0'), ord('9') + 1))
        str += ch
    return str

# 发送验证码邮件
@normal_bp.route('/send-email', methods=['POST'])
def send():
    # 渲染邮件
    to_email = request.form.get('user_email')
    username = request.form.get('user_name')
    ver_code = generate_ver_code()
    send_email(to_mail=to_email, subject='Captcha', template='verifyCode', username=username, ver_code=ver_code)
    # 判断是否已经存在一个最新的可用的验证码,以确保生效的验证码是用户收到最新邮件中的验证码
    exist_code = VerifyCode.query.filter(VerifyCode.who == to_email, VerifyCode.is_work == 1).order_by(VerifyCode.timestamps.desc()).first()
    if exist_code:
        exist_code.is_work = False
    # 验证码写数据库
    nt = datetime.datetime.now()
    et = nt + datetime.timedelta(minutes=10)
    verify_code = VerifyCode(val=ver_code, who=to_email, expire_time=et)
    db.session.add(verify_code)
    db.session.commit()
    return jsonify({'tag': 1, 'info': '邮件发送成功!'})

@normal_bp.route('/image/upload/')
def image_upload():
    pass
