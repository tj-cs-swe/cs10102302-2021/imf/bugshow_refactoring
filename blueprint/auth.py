from flask import Blueprint, render_template
from flask_login import current_user, login_user, logout_user, login_required
from sqlalchemy import or_
from flask import flash, request, redirect, url_for
from extensions import db
from settings import BaseConfig
from models import *
from forms import *
from utils import *
import os

# 账户管理蓝图
auth_bp = Blueprint('auth', __name__, url_prefix='/auth')

# 注册
@auth_bp.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        role_id = form.user_role.data
        username = form.user_name.data
        nickname = form.nickname.data
        password = form.confirm_pwd.data
        email = form.user_email.data
        captcha = request.form.get('captcha')
        code = VerifyCode.query.filter(VerifyCode.who == email, VerifyCode.is_work == 1).order_by(VerifyCode.timestamps.desc()).first()
        if code:
            if code.val != int(captcha):
                flash('验证码错误!', 'danger')
                return redirect(request.referrer)
            elif code.expire_time < datetime.datetime.now():
                flash('验证码已过期!', 'danger')
                return redirect(request.referrer)
        else:
            flash('请先发送验证码到邮箱!', 'info')
            return redirect(request.referrer)

        user = User(role_id=role_id, username=username,nickname=nickname,email=email,password=password)
        user.generate_avatar() # 初始化头像
        user.set_password(password)
        code.is_work = False
        db.session.add(user)
        db.session.commit()
        flash('注册成功,欢迎加入BugShow!', 'success')
        return redirect(url_for('.login'))
    return render_template('register.html', template_folder='templates', form=form)

# 登录
@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('/'))
    form = LoginForm()
    if form.validate_on_submit():
        usr = form.usr_email.data
        pwd = form.password.data
        user = User.query.filter(or_(User.username == usr, User.email == usr.lower())).first()
        if user is not None and user.check_password(pwd):
            if login_user(user, form.remember_me.data):
                flash('登录成功!', 'success')
                return redirect(url_for('index_bp.index'))
        elif user is None:
            flash('无效的邮箱或用户名.', 'danger')
        else:
            flash('无效的密码', 'danger')
    return render_template('login.html', template_folder='templates', form=form)

# 修改密码
@auth_bp.route('/change-passwd', methods=['GET', 'POST'])
def ChangePassword():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            usr = current_user.id
        old_password = form.old_password.data
        passwd = form.password1.data
        user = User.query.filter(or_(User.id == usr)).first()
        # 旧密码正确
        if user.check_password(old_password):
            user.set_password(passwd)
            db.session.commit()
            flash('修改密码成功!', 'success')
            return redirect(url_for('index_bp.index'))
    return render_template('change-passwd.html', template_folder='templates', form=form)

# 修改头像上传
@auth_bp.route('/change-avatar', methods=['GET', 'POST'])
@login_required
def ChangeAvatar():
    form = ChangeAvatarForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            usr = current_user.id
        # 上传头像文件
        user = User.query.filter(or_(User.id == usr)).first()
        avatar = form.image.data
        upload_path = BaseConfig.AVATARS_SAVE_PATH + avatar.filename
        avatar.save(upload_path)
        user.set_avatar("resources/avatars/"+avatar.filename)
        db.session.commit()
        flash('修改头像成功!', 'success')
        return redirect(url_for('index_bp.index'))
    return render_template('change-avatar.html',template_folder='templates',form=form)

# 注销
@auth_bp.route('/logout', methods=['GET'])
def logout():
    logout_user()
    flash('退出成功!', 'success')
    return redirect(url_for('index_bp.index'))
