from threading import Thread
from extensions import mail
from flask_mail import Message
from flask import current_app, render_template

# 利用flask的app_context应用上下文，异步发送邮件
def async_send_mail(app, msg):
    with app.app_context():
        mail.send(msg)

# 初始化验证码邮件，建立发送邮件线程
def send_email(to_mail, subject, template, **kwargs):
    # 邮件内容参数导入
    message = Message(current_app.config['BUGSHOW_MAIL_SUBJECT_PRE'] + subject, recipients=[to_mail], sender=current_app.config['MAIL_USERNAME'])
    # 邮件效果渲染
    message.body = render_template(template + '.txt', **kwargs)
    message.html = render_template(template + '.html', **kwargs)
    # 建立发送线程
    th = Thread(target=async_send_mail, args=(current_app._get_current_object(), message))
    th.start()
    return th
