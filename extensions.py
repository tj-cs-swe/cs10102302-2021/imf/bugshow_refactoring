from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
from flask_ckeditor import CKEditor
from flask_moment import Moment

# 建立SQLAlchemy数据库对象db，main.py中用pymysql的api与MySQL建立连接
db = SQLAlchemy()
# 建立flask的邮箱对象，发验证码
mail = Mail()
# 建立flask的登录管理对象
login_manager = LoginManager()
# 建立内置的ck编辑器对象
ckeditor = CKEditor()
# 建立flask-moment对象
moment = Moment()

# 根据id加载用户全部信息返回对象
@login_manager.user_loader
def load_user(user_id):
    from models import User
    user = User.query.filter_by(id=user_id).first()
    return user

# 登录管理视图
login_manager.login_view = 'auth.login'
login_manager.login_message = u'请先登陆!'
login_manager.login_message_category = 'danger'
